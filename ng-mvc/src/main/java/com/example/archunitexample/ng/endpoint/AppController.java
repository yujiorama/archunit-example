package com.example.archunitexample.ng.endpoint;

import com.example.archunitexample.ng.dao.AppDao;
import com.example.archunitexample.ng.service.AppService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    private final AppService appService;

    private final AppDao appDao;

    public AppController(AppService appService, AppDao appDao) {
        this.appService = appService;
        this.appDao = appDao;
    }

    @GetMapping("/hello")
    public String hello() {

        String message = this.appDao.findMessage();

        return message + this.appService.getMessage();
    }
}
