package com.example.archunitexample.ng.service;

import com.example.archunitexample.ng.dao.AppDao;
import com.example.archunitexample.ng.endpoint.AppController;
import org.springframework.stereotype.Service;

@Service
public class AppService {

    private final AppController appController;
    private final AppDao appDao;


    public AppService(AppController appController, AppDao appDao) {
        this.appController = appController;
        this.appDao = appDao;
    }

    public String getMessage() {

        return String.format("hello %s", this.appDao.findMessage());
    }
}
