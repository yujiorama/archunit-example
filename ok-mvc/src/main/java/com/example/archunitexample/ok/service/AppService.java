package com.example.archunitexample.ok.service;

import com.example.archunitexample.ok.dao.AppDao;
import org.springframework.stereotype.Service;

@Service
public class AppService {

    private final AppDao appDao;


    public AppService(AppDao appDao) {
        this.appDao = appDao;
    }

    public String getMessage() {

        return String.format("hello %s", this.appDao.findMessage());
    }
}
