package com.example.archunitexample.ok.endpoint;

import com.example.archunitexample.ok.service.AppService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    private final AppService appService;

    public AppController(AppService appService) {
        this.appService = appService;
    }

    @GetMapping("/hello")
    public String hello() {

        return this.appService.getMessage();
    }
}
