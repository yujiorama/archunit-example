package com.example.archunitexample.ok.dao;

import org.springframework.stereotype.Component;

@Component
public class AppDao {

    public String findMessage() {
        return "app";
    }
}
